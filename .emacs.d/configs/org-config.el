(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
(add-hook 'org-mode-hook 'git-auto-commit-mode) ; auto commit org files

(setq gac-automatically-push-p t)
(setq gac-ask-for-summary-p t)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(setq org-timer-default-timer 25)


;; ;; (require 'org-s5)
;; (add-to-list 'load-path "~/.emacs.d/el-get/org-mode/lisp/")
;; (add-to-list 'load-path "~/.emacs.d/el-get/org-mode/contrib/lisp" t)
;; ;;(require 'org-special-blocks)
(setq org-export-with-sub-superscripts nil)
(unless (boundp 'org-export-latex-classes)
  (setq org-export-latex-classes nil))
(add-to-list 'org-export-latex-classes
             '("book"
               "\\documentclass{book}
               \\usepackage[utf8x]{inputenc}
               \\usepackage[T1]{fontenc}
               \\usepackage[french]{babel}
               \\usepackage[colorlinks=true,urlcolor=SteelBlue4,linkcolor=Firebrick4]{hyperref}
               \\newsavebox{\\mybox}
               \\newlength{\\mydepth}
               \\newlength{\\myheight}


               \\newenvironment{REMARQUE}
                {\\begin{lrbox}{\\mybox}\\begin{minipage}{0.7\\textwidth}}%
                {\\end{minipage}\\end{lrbox}
                \\settodepth{\\mydepth}{\\usebox{\\mybox}}%
                \\settoheight{\\myheight}{\\usebox{\\mybox}}%
                \\addtolength{\\myheight}{\\mydepth}%
                \\noindent\\makebox[90pt]{\\hspace{-0pt}\\rule[-\\mydepth]{5pt}{\\myheight}}%
                \\usebox{\\mybox}}
                "
               ("\\part{%s}" . "\\part*{%s}")
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ))



(setq org-agenda-files (quote ("~/.emacs.d/org-list/work.org"
                               "~/.emacs.d/org-list/perso.org"
                               "~/.emacs.d/org-list/manual.org"
                               "~/.emacs.d/org-list/index.org"
                               )))

(setq org-directory "/home/yohann/.emacs.d/org-list/")


(setq org-todo-keywords
  '((sequence "TODO" "IN-PROGRESS" "WAITING" "DONE")))

(setq org-tag-alist '(
                      ("@work" . ?w) ("@home" . ?h) ("@perso" . ?p) ("@manual" . ?m)
                      ))

(setq org-directory "~/.emacs.d/org-list/")
(setq org-mobile-directory "~/Dropbox/MobileOrg")
(setq org-mobile-inbox-for-pull "~/.emacs.d/org-list/flagged.org")
(setq org-combined-agenda-icalendar-file "~/.emacs.d/org-list/agenda.ics")

(setq org-export-latex-emphasis-alist
      '(("*" "\\textbf{%s}" nil)
        ("/" "\\emph{%s}" nil)
        ("_" "\\underline{%s}" nil)
        ("+" "\\st{%s}" nil)
        ("=" "\\url{%s}" nil)
        ("~" "\\verb" t)))


;; Org-exporter

;;reveal

(setq org-reveal-root "file:///home/yohann/.emacs.d/org-list/reveal.js")
(require 'ox-reveal)
(require 'ox-md)
(require 'ox-rst)
;;



;; active Babel languages

(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (python . t)
   (sh . t)
   (ditaa . t)
   (sql . t)
   (dot . t)
   (gnuplot . t)
   ))

(setq org-confirm-babel-evaluate nil)


(defun auto-commit-files (list)
  (interactive
    (list (list (buffer-file-name (current-buffer)))))
  "LIST to be auto commit"
  (while list
    (let* ((file (car list))
           (file-buffer (get-file-buffer file)))
      (when file-buffer
        (set-buffer file-buffer)
        (when (magit-anything-modified-p nil file)
          (magit-call-git "add" file)
          (magit-call-git "commit" "-m" (concat file " update"))
          (magit-call-git "push" "origin")
          (magit-refresh)
          (print (concat file "is pushed!!!")))))
    (setq list (cdr list))))



(setq org-babel-python-command "python3")


(provide 'org-config)
