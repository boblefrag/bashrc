install :
	ls -a
	cp .bash_aliases ~/
	cp .bashrc ~/
	cp -r .config ~/
	cp -r .emacs ~/
	cp -r .emacs.d ~/
	cp run_once.lua ~/
	cp wonderwoman.jpeg ~/Images
