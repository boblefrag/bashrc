T 1515150534 18<*status>	No such module [replaybuffer]
T 1515151058 20<boblefrag>30	LoadMod playback
T 1515151058 18<*status>	Unable to find modinfo [playback] [Unable to find module [playback]]
T 1515151066 20<boblefrag>30	LoadMod replaybuffer
T 1515151066 18<*status>	Unable to find modinfo [replaybuffer] [Unable to find module [replaybuffer]]
T 1515151360 20<boblefrag>30	help
T 1515151360 18<*status>	In the following list all occurrences of <#chan> support wildcards (* and ?)
T 1515151360 18<*status>	(Except ListNicks)
T 1515151360 18<*status>	+------------------------+---------------------------------------+----------------------------------------------------------------------------------+
T 1515151360 18<*status>	| Command                | Arguments                             | Description                                                                      |
T 1515151360 18<*status>	+------------------------+---------------------------------------+----------------------------------------------------------------------------------+
T 1515151360 18<*status>	| Version                |                                       | Print which version of ZNC this is                                               |
T 1515151360 18<*status>	| ListMods               |                                       | List all loaded modules                                                          |
T 1515151360 18<*status>	| ListAvailMods          |                                       | List all available modules                                                       |
T 1515151360 18<*status>	| ListNicks              | <#chan>                               | List all nicks on a channel                                                      |
T 1515151360 18<*status>	| ListServers            |                                       | List all servers of current IRC network                                          |
T 1515151360 18<*status>	| AddNetwork             | <name>                                | Add a network to your user                                                       |
T 1515151360 18<*status>	| DelNetwork             | <name>                                | Delete a network from your user                                                  |
T 1515151360 18<*status>	| ListNetworks           |                                       | List all networks                                                                |
T 1515151360 18<*status>	| MoveNetwork            | old-user old-net new-user [new-net]   | Move an IRC network from one user to another                                     |
T 1515151360 18<*status>	| JumpNetwork            | <network>                             | Jump to another network                                                          |
T 1515151360 18<*status>	| AddServer              | <host> [[+]port] [pass]               | Add a server to the list of alternate/backup servers of current IRC network.     |
T 1515151360 18<*status>	| DelServer              | <host> [port] [pass]                  | Remove a server from the list of alternate/backup servers of current IRC network |
T 1515151360 18<*status>	| Enablechan             | <#chan>                               | Enable the channel                                                               |
T 1515151360 18<*status>	| Detach                 | <#chan>                               | Detach from the channel                                                          |
T 1515151360 18<*status>	| Topics                 |                                       | Show topics in all your channels                                                 |
T 1515151360 18<*status>	| PlayBuffer             | <#chan>                               | Play back the buffer for a given channel                                         |
T 1515151360 18<*status>	| ClearBuffer            | <#chan>                               | Clear the buffer for a given channel                                             |
T 1515151360 18<*status>	| ClearAllChannelBuffers |                                       | Clear the channel buffers                                                        |
T 1515151360 18<*status>	| SetBuffer              | <#chan> [linecount]                   | Set the buffer count for a channel                                               |
T 1515151360 18<*status>	| AddBindHost            | <host (IP preferred)>                 | Adds a bind host for normal users to use                                         |
T 1515151360 18<*status>	| DelBindHost            | <host>                                | Removes a bind host from the list                                                |
T 1515151360 18<*status>	| ListBindHosts          |                                       | Shows the configured list of bind hosts                                          |
T 1515151360 18<*status>	| SetBindHost            | <host (IP preferred)>                 | Set the bind host for this connection                                            |
T 1515151360 18<*status>	| SetUserBindHost        | <host (IP preferred)>                 | Set the default bind host for this user                                          |
T 1515151360 18<*status>	| ClearBindHost          |                                       | Clear the bind host for this connection                                          |
T 1515151360 18<*status>	| ClearUserBindHost      |                                       | Clear the default bind host for this user                                        |
T 1515151360 18<*status>	| ShowBindHost           |                                       | Show currently selected bind host                                                |
T 1515151360 18<*status>	| Jump [server]          |                                       | Jump to the next or the specified server                                         |
T 1515151360 18<*status>	| Disconnect             | [message]                             | Disconnect from IRC                                                              |
T 1515151360 18<*status>	| Connect                |                                       | Reconnect to IRC                                                                 |
T 1515151360 18<*status>	| Uptime                 |                                       | Show for how long ZNC has been running                                           |
T 1515151360 18<*status>	| LoadMod                | [--type=global|user|network] <module> | Load a module                                                                    |
T 1515151360 18<*status>	| UnloadMod              | [--type=global|user|network] <module> | Unload a module                                                                  |
T 1515151360 18<*status>	| ReloadMod              | [--type=global|user|network] <module> | Reload a module                                                                  |
T 1515151360 18<*status>	| UpdateMod              | <module>                              | Reload a module everywhere                                                       |
T 1515151360 18<*status>	| ShowMOTD               |                                       | Show ZNC's message of the day                                                    |
T 1515151360 18<*status>	| SetMOTD                | <Message>                             | Set ZNC's message of the day                                                     |
T 1515151360 18<*status>	| AddMOTD                | <Message>                             | Append <Message> to ZNC's MOTD                                                   |
T 1515151360 18<*status>	| ClearMOTD              |                                       | Clear ZNC's MOTD                                                                 |
T 1515151360 18<*status>	| ListPorts              |                                       | Show all active listeners                                                        |
T 1515151360 18<*status>	| AddPort                | <arguments>                           | Add another port for ZNC to listen on                                            |
T 1515151360 18<*status>	| DelPort                | <arguments>                           | Remove a port from ZNC                                                           |
T 1515151360 18<*status>	| Rehash                 |                                       | Reload znc.conf from disk                                                        |
T 1515151360 18<*status>	| SaveConfig             |                                       | Save the current settings to disk                                                |
T 1515151360 18<*status>	| ListUsers              |                                       | List all ZNC users and their connection status                                   |
T 1515151360 18<*status>	| ListAllUserNetworks    |                                       | List all ZNC users and their networks                                            |
T 1515151360 18<*status>	| ListChans              | [User <network>]                      | List all channels                                                                |
T 1515151360 18<*status>	| ListClients            | [User]                                | List all connected clients                                                       |
T 1515151360 18<*status>	| Traffic                |                                       | Show basic traffic stats for all ZNC users                                       |
T 1515151360 18<*status>	| Broadcast              | [message]                             | Broadcast a message to all ZNC users                                             |
T 1515151360 18<*status>	| Shutdown               | [message]                             | Shut down ZNC completely                                                         |
T 1515151360 18<*status>	| Restart                | [message]                             | Restart ZNC                                                                      |
T 1515151360 18<*status>	+------------------------+---------------------------------------+----------------------------------------------------------------------------------+
T 1515151382 20<boblefrag>30	ListAvailMods
T 1515151393 Utilisation : MSG <pseudo> <message>, envoie un message privé, envoyez un message à « . » pour envoyer au dernier pseudo utilisé ou préfixez avec « = » pour un chat DCC
T 1515151397 20<boblefrag>30	ListAvailMods
T 1515151424 18<*status>	Global modules:
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	| Name            | Description                                                         |
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	|  adminlog       | Log ZNC events to file and/or syslog.                               |
T 1515151424 18<*status>	|  block_motd     | Block the MOTD from IRC so it's not sent to your client(s).         |
T 1515151424 18<*status>	|  blockuser      | Block certain users from logging in                                 |
T 1515151424 18<*status>	|  certauth       | Allow users to authenticate via SSL client certificates             |
T 1515151424 18<*status>	|  cyrusauth      | Allow users to authenticate via SASL password verification method   |
T 1515151424 18<*status>	|  fail2ban       | Block IPs for some time after a failed login                        |
T 1515151424 18<*status>	|  identfile      | Write the ident of a user to a file when they are trying to connect |
T 1515151424 18<*status>	|  imapauth       | Allow users to authenticate via imap                                |
T 1515151424 18<*status>	|  lastseen       | Collects data about when a user last logged in                      |
T 1515151424 18<*status>	|  log            | Write IRC logs                                                      |
T 1515151424 18<*status>	|  modperl        | Loads perl scripts as ZNC modules                                   |
T 1515151424 18<*status>	|  modpython      | Loads python scripts as ZNC modules                                 |
T 1515151424 18<*status>	|  notify_connect | Notifies all admin users when a client connects or disconnects.     |
T 1515151424 18<*status>	|  partyline      | Internal channels and queries for users connected to znc            |
T 1515151424 18<*status>	|  webadmin       | Web based administration module                                     |
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	User modules:
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	| Name              | Description                                                                              |
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	|  autoattach       | Reattaches you to channels on activity.                                                  |
T 1515151424 18<*status>	|  autoreply        | Reply to queries when you are away                                                       |
T 1515151424 18<*status>	|  block_motd       | Block the MOTD from IRC so it's not sent to your client(s).                              |
T 1515151424 18<*status>	|  bouncedcc        | Bounces DCC transfers through ZNC instead of sending them directly to the user.          |
T 1515151424 18<*status>	|  buffextras       | Add joins, parts etc. to the playback buffer                                             |
T 1515151424 18<*status>	|  cert             | Use a ssl certificate to connect to a server                                             |
T 1515151424 18<*status>	| *chansaver        | Keep config up-to-date when user joins/parts                                             |
T 1515151424 18<*status>	|  charset          | Normalizes character encodings.                                                          |
T 1515151424 18<*status>	|  clearbufferonmsg | Clear all channel buffers whenever the user does something                               |
T 1515151424 18<*status>	|  clientnotify     | Notifies you when another IRC client logs into or out of your account. Configurable.     |
T 1515151424 18<*status>	| *controlpanel     | Dynamic configuration through IRC. Allows editing only yourself if you're not ZNC admin. |
T 1515151424 18<*status>	|  ctcpflood        | Don't forward CTCP floods to clients                                                     |
T 1515151424 18<*status>	|  dcc              | This module allows you to transfer files to and from ZNC                                 |
T 1515151424 18<*status>	|  disconkick       | Kicks the client from all channels when the connection to the IRC server is lost         |
T 1515151424 18<*status>	|  flooddetach      | Detach channels when flooded                                                             |
T 1515151424 18<*status>	|  listsockets      | List active sockets                                                                      |
T 1515151424 18<*status>	|  log              | Write IRC logs                                                                           |
T 1515151424 18<*status>	|  missingmotd      | Sends 422 to clients when they login                                                     |
T 1515151424 18<*status>	|  notes            | Keep and replay notes                                                                    |
T 1515151424 18<*status>	| *perform          | Keeps a list of commands to be executed when ZNC connects to IRC.                        |
T 1515151424 18<*status>	|  raw              | View all of the raw traffic                                                              |
T 1515151424 18<*status>	|  sample           | To be used as a sample for writing modules                                               |
T 1515151424 18<*status>	|  send_raw         | Lets you send some raw IRC lines as/to someone else                                      |
T 1515151424 18<*status>	|  shell            | Gives shell access. Only ZNC admins can use it.                                          |
T 1515151424 18<*status>	| *webadmin         | Web based administration module                                                          |
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	Network modules:
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	| Name            | Description                                                                                     |
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	|  autocycle      | Rejoins channels to gain Op if you're the only user left                                        |
T 1515151424 18<*status>	|  autoop         | Auto op the good guys                                                                           |
T 1515151424 18<*status>	|  autoreply      | Reply to queries when you are away                                                              |
T 1515151424 18<*status>	|  autovoice      | Auto voice the good guys                                                                        |
T 1515151424 18<*status>	|  awaynick       | Change your nick while you are away                                                             |
T 1515151424 18<*status>	|  awaystore      | Adds auto-away with logging, useful when you use ZNC from different locations                   |
T 1515151424 18<*status>	|  block_motd     | Block the MOTD from IRC so it's not sent to your client(s).                                     |
T 1515151424 18<*status>	|  cert           | Use a ssl certificate to connect to a server                                                    |
T 1515151424 18<*status>	| *chansaver      | Keep config up-to-date when user joins/parts                                                    |
T 1515151424 18<*status>	|  crypt          | Encryption for channel/private messages                                                         |
T 1515151424 18<*status>	| *keepnick       | Keep trying for your primary nick                                                               |
T 1515151424 18<*status>	| *kickrejoin     | Autorejoin on kick                                                                              |
T 1515151424 18<*status>	|  log            | Write IRC logs                                                                                  |
T 1515151424 18<*status>	|  modtcl         | Loads Tcl scripts as ZNC modules                                                                |
T 1515151424 18<*status>	|  modules_online | Make ZNC's *modules to be "online".                                                             |
T 1515151424 18<*status>	| *nickserv       | Auths you with NickServ                                                                         |
T 1515151424 18<*status>	|  perform        | Keeps a list of commands to be executed when ZNC connects to IRC.                               |
T 1515151424 18<*status>	|  q              | Auths you with QuakeNet's Q bot.                                                                |
T 1515151424 18<*status>	|  raw            | View all of the raw traffic                                                                     |
T 1515151424 18<*status>	|  route_replies  | Send replies (e.g. to /who) to the right client only                                            |
T 1515151424 18<*status>	|  sasl           | Adds support for sasl authentication capability to authenticate to an IRC server                |
T 1515151424 18<*status>	|  savebuff       | Stores channel buffers to disk, encrypted                                                       |
T 1515151424 18<*status>	|  schat          | Secure cross platform (:P) chat system                                                          |
T 1515151424 18<*status>	|  simple_away    | This module will automatically set you away on IRC while you are disconnected from the bouncer. |
T 1515151424 18<*status>	|  stickychan     | configless sticky chans, keeps you there very stickily even                                     |
T 1515151424 18<*status>	|  watch          | Copy activity from a specific user into a separate window                                       |
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	Global modules:
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	| Name            | Description                                                         |
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	|  adminlog       | Log ZNC events to file and/or syslog.                               |
T 1515151424 18<*status>	|  block_motd     | Block the MOTD from IRC so it's not sent to your client(s).         |
T 1515151424 18<*status>	|  blockuser      | Block certain users from logging in                                 |
T 1515151424 18<*status>	|  certauth       | Allow users to authenticate via SSL client certificates             |
T 1515151424 18<*status>	|  cyrusauth      | Allow users to authenticate via SASL password verification method   |
T 1515151424 18<*status>	|  fail2ban       | Block IPs for some time after a failed login                        |
T 1515151424 18<*status>	|  identfile      | Write the ident of a user to a file when they are trying to connect |
T 1515151424 18<*status>	|  imapauth       | Allow users to authenticate via imap                                |
T 1515151424 18<*status>	|  lastseen       | Collects data about when a user last logged in                      |
T 1515151424 18<*status>	|  log            | Write IRC logs                                                      |
T 1515151424 18<*status>	|  modperl        | Loads perl scripts as ZNC modules                                   |
T 1515151424 18<*status>	|  modpython      | Loads python scripts as ZNC modules                                 |
T 1515151424 18<*status>	|  notify_connect | Notifies all admin users when a client connects or disconnects.     |
T 1515151424 18<*status>	|  partyline      | Internal channels and queries for users connected to znc            |
T 1515151424 18<*status>	|  webadmin       | Web based administration module                                     |
T 1515151424 18<*status>	+-----------------+---------------------------------------------------------------------+
T 1515151424 18<*status>	User modules:
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	| Name              | Description                                                                              |
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	|  autoattach       | Reattaches you to channels on activity.                                                  |
T 1515151424 18<*status>	|  autoreply        | Reply to queries when you are away                                                       |
T 1515151424 18<*status>	|  block_motd       | Block the MOTD from IRC so it's not sent to your client(s).                              |
T 1515151424 18<*status>	|  bouncedcc        | Bounces DCC transfers through ZNC instead of sending them directly to the user.          |
T 1515151424 18<*status>	|  buffextras       | Add joins, parts etc. to the playback buffer                                             |
T 1515151424 18<*status>	|  cert             | Use a ssl certificate to connect to a server                                             |
T 1515151424 18<*status>	| *chansaver        | Keep config up-to-date when user joins/parts                                             |
T 1515151424 18<*status>	|  charset          | Normalizes character encodings.                                                          |
T 1515151424 18<*status>	|  clearbufferonmsg | Clear all channel buffers whenever the user does something                               |
T 1515151424 18<*status>	|  clientnotify     | Notifies you when another IRC client logs into or out of your account. Configurable.     |
T 1515151424 18<*status>	| *controlpanel     | Dynamic configuration through IRC. Allows editing only yourself if you're not ZNC admin. |
T 1515151424 18<*status>	|  ctcpflood        | Don't forward CTCP floods to clients                                                     |
T 1515151424 18<*status>	|  dcc              | This module allows you to transfer files to and from ZNC                                 |
T 1515151424 18<*status>	|  disconkick       | Kicks the client from all channels when the connection to the IRC server is lost         |
T 1515151424 18<*status>	|  flooddetach      | Detach channels when flooded                                                             |
T 1515151424 18<*status>	|  listsockets      | List active sockets                                                                      |
T 1515151424 18<*status>	|  log              | Write IRC logs                                                                           |
T 1515151424 18<*status>	|  missingmotd      | Sends 422 to clients when they login                                                     |
T 1515151424 18<*status>	|  notes            | Keep and replay notes                                                                    |
T 1515151424 18<*status>	| *perform          | Keeps a list of commands to be executed when ZNC connects to IRC.                        |
T 1515151424 18<*status>	|  raw              | View all of the raw traffic                                                              |
T 1515151424 18<*status>	|  sample           | To be used as a sample for writing modules                                               |
T 1515151424 18<*status>	|  send_raw         | Lets you send some raw IRC lines as/to someone else                                      |
T 1515151424 18<*status>	|  shell            | Gives shell access. Only ZNC admins can use it.                                          |
T 1515151424 18<*status>	| *webadmin         | Web based administration module                                                          |
T 1515151424 18<*status>	+-------------------+------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	Network modules:
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	| Name            | Description                                                                                     |
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151424 18<*status>	|  autocycle      | Rejoins channels to gain Op if you're the only user left                                        |
T 1515151424 18<*status>	|  autoop         | Auto op the good guys                                                                           |
T 1515151424 18<*status>	|  autoreply      | Reply to queries when you are away                                                              |
T 1515151424 18<*status>	|  autovoice      | Auto voice the good guys                                                                        |
T 1515151424 18<*status>	|  awaynick       | Change your nick while you are away                                                             |
T 1515151424 18<*status>	|  awaystore      | Adds auto-away with logging, useful when you use ZNC from different locations                   |
T 1515151424 18<*status>	|  block_motd     | Block the MOTD from IRC so it's not sent to your client(s).                                     |
T 1515151424 18<*status>	|  cert           | Use a ssl certificate to connect to a server                                                    |
T 1515151424 18<*status>	| *chansaver      | Keep config up-to-date when user joins/parts                                                    |
T 1515151424 18<*status>	|  crypt          | Encryption for channel/private messages                                                         |
T 1515151424 18<*status>	| *keepnick       | Keep trying for your primary nick                                                               |
T 1515151424 18<*status>	| *kickrejoin     | Autorejoin on kick                                                                              |
T 1515151424 18<*status>	|  log            | Write IRC logs                                                                                  |
T 1515151424 18<*status>	|  modtcl         | Loads Tcl scripts as ZNC modules                                                                |
T 1515151424 18<*status>	|  modules_online | Make ZNC's *modules to be "online".                                                             |
T 1515151424 18<*status>	| *nickserv       | Auths you with NickServ                                                                         |
T 1515151424 18<*status>	|  perform        | Keeps a list of commands to be executed when ZNC connects to IRC.                               |
T 1515151424 18<*status>	|  q              | Auths you with QuakeNet's Q bot.                                                                |
T 1515151424 18<*status>	|  raw            | View all of the raw traffic                                                                     |
T 1515151424 18<*status>	|  route_replies  | Send replies (e.g. to /who) to the right client only                                            |
T 1515151424 18<*status>	|  sasl           | Adds support for sasl authentication capability to authenticate to an IRC server                |
T 1515151424 18<*status>	|  savebuff       | Stores channel buffers to disk, encrypted                                                       |
T 1515151424 18<*status>	|  schat          | Secure cross platform (:P) chat system                                                          |
T 1515151424 18<*status>	|  simple_away    | This module will automatically set you away on IRC while you are disconnected from the bouncer. |
T 1515151424 18<*status>	|  stickychan     | configless sticky chans, keeps you there very stickily even                                     |
T 1515151424 18<*status>	|  watch          | Copy activity from a specific user into a separate window                                       |
T 1515151424 18<*status>	+-----------------+-------------------------------------------------------------------------------------------------+
T 1515151439 20<boblefrag>30	PlayBuffer #chezleo
T 1515151439 18<*status>	The buffer for [#chezleo] is empty
T 1515151457 20<boblefrag>30	PlayBuffer #pico8
T 1515151457 18<*status>	The buffer for [#pico8] is empty
T 1515151465 20<boblefrag>30	PlayBuffer #emacs
T 1515151466 18<*status>	The buffer for [#emacs] is empty
