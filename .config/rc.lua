-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local run_once = require("run_once")
local path_to_icons = "/usr/share/icons/Adwaita/scalable/status/"

-- Load Debian menu entries
require("debian.menu")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- {{{ Spawn useful things aka get ready to work

awful.util.spawn_with_shell("unagi &") -- transparency rocks
awful.util.spawn_with_shell("/opt/google/chrome/google-chrome --profile-directory=Default --app-id=knipolnnllmklapflnccelgolnpehhpl")
awful.util.spawn("redshift")
awful.util.spawn("iceweasel")
awful.util.spawn("gnome-terminal")
awful.util.spawn("spotify")
awful.util.spawn("hexchat")
awful.util.spawn("emacs")
awful.util.spawn("evolution")

-- }}

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init("/home/yohann/.config/awesome/theme.lua")

-- This is used later as the default terminal and editor to run.

terminal = "x-terminal-emulator"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = editor


-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier

}
-- }}}


-- {{{ Widget definition

-- wifi
-- an icon
 wifi_signal_image = wibox.widget.imagebox()
 wifi_signal_image:set_image(path_to_icons .. "network-wireless-signal-excellent-symbolic.svg")

function wifiInfo()
     awful.spawn.easy_async("awk 'NR==3 {printf \"%.1f%%\\n\",($3/70)*100}' /proc/net/wireless",
          function(stdout, stderr, reason, exit_code)

              local wifiStrength = stdout

                  if wifiStrength == "" then
                      wifi_signal_image:set_image(
                          path_to_icons .. "network-wireless-offline-symbolic.svg")

                  else
                      local strength = tonumber(string.sub(wifiStrength, 0, 4))

                  if strength<30 then
                      wifi_signal_image:set_image(path_to_icons .. "network-wireless-signal-weak-symbolic.svg")
                  elseif strength<50 then
                      wifi_signal_image:set_image(path_to_icons .. "network-wireless-signal-ok-symbolic.svg")
                  elseif strength<90 then
                       wifi_signal_image:set_image(path_to_icons .. "network-wireless-signal-good-symbolic.svg")
                  else
                       wifi_signal_image:set_image(path_to_icons .. "network-wireless-signal-excellent-symbolic.svg")
                  end
               end
           end)
       end
 wifiInfo()

wifi_timer = timer({timeout=2})
wifi_timer:connect_signal("timeout",wifiInfo)
wifi_timer:start()

-- wifi tooltip

wifi_t = awful.tooltip({
    objects = { wifi_signal_image },
    timer_function = function()
        f = io.popen("awk 'NR==3 {printf \"%.1f%%\\n\",($3/70)*100}' /proc/net/wireless")
        return f:read('*l')
        end,
    })


-- Battery level

-- 2 bat so 2 images
batterywidget_image1 = wibox.widget.imagebox()
batterywidget_image2 = wibox.widget.imagebox()

batterywidgettimer = timer({ timeout = 5 })

function update_bat(widget, info)
    if info < 10 then
       widget:set_image(path_to_icons .. "battery-empty-symbolic.svg")
    elseif info < 25 then
       widget:set_image(path_to_icons .. "battery-caution-symbolic.svg")
    elseif info < 40 then
       widget:set_image(path_to_icons .. "battery-low-symbolic.svg")
    elseif info < 95 then
       widget:set_image(path_to_icons .. "battery-good-symbolic.svg")
    else
        widget:set_image(path_to_icons .. "battery-full-symbolic.svg")
    end
end

batterywidgettimer:connect_signal("timeout",
  function()
    fh = assert(io.popen("acpi | cut -d, -f 2,3 -", "r"))

    local bat1 = string.gsub(string.sub(fh:read("*l"), 2, 4), "(%\%)", "")
    local bat2 = string.gsub(string.sub(fh:read("*l"), 2, 4), "(%\%)", "")

    update_bat(batterywidget_image1, tonumber(bat1))
    update_bat(batterywidget_image2, tonumber(bat2))
    fh:close()
  end
)

batterywidgettimer:start()

-- battery tooltip

battery_t = awful.tooltip({
    objects = { batterywidget_image1 },
    timer_function = function()
    fh = assert(io.popen("acpi | cut -d, -f 2,3 -", "r"))
    local bat1 = string.gsub(string.sub(fh:read("*l"), 2, 4), "(%\%)", "")
    local bat2 = string.gsub(string.sub(fh:read("*l"), 2, 4), "(%\%)", "")

    --local bat1 = fh:read("*l")
    --local bat2 = fh:read("*l")

            return "BAT1: " ..bat1 .."\n" .. "BAT2: " .. bat2
        end,
    })


-- volume
-- an image
volume_widget = wibox.widget.imagebox()
-- and a progressbar
volume_widget_bar = wibox.widget {
       {
           max_value     = 1,
           value         = 0.33,
           widget        = wibox.widget.progressbar,
           color         = "#CBCBCB",
           background_color = "#000000"
       },
       forced_height = 100,
       forced_width  = 10,
       direction     = 'east',
       layout        = wibox.container.rotate,

   }

volume_widget_bar.widget:set_value(1)


function update_volume(widget)
   awful.spawn.easy_async("amixer get Master",
       function(stdout)
           local mixer = stdout
           local volume, mute = string.match(mixer, "([%d]+)%%.*%[([%l]*)")
           volume_widget_bar.widget:set_value(tonumber(volume)/100)
           if mute == "off" or tonumber(volume) == 0 then
           widget:set_image(path_to_icons .. "audio-volume-muted-symbolic.svg")
           volume_widget_bar.widget:set_value(0)
           elseif tonumber(volume) < 30 then
           widget:set_image(path_to_icons .. "audio-volume-low-symbolic.svg")
           elseif tonumber(volume) < 70 then
           widget:set_image(path_to_icons .. "audio-volume-medium-symbolic.svg")
           else
           widget:set_image(path_to_icons .. "audio-volume-high-symbolic.svg")
           end
   end)
end


-- volume tooltip

volume_t = awful.tooltip({
    objects = { volume_widget },
    timer_function = function()
       local f = io.popen("amixer get Master")
       local mixer = f:read("*a")
       local volume, mute = string.match(mixer, "([%d]+)%%.*%[([%l]*)")
            return volume .. "%"
        end,
    })

update_volume(volume_widget)

mytimer = timer({ timeout = 1 })
mytimer:connect_signal("timeout", function () update_volume(volume_widget) end)
mytimer:start()

-- Brightness

bright_image = wibox.widget.imagebox()
bright_image:set_image(path_to_icons .. "display-brightness-symbolic.svg")

bright_widget = wibox.widget {
       {
           max_value     = 1,
           value         = 0.33,
           widget        = wibox.widget.progressbar,
           color         = "#CBCBCB",
           background_color = "#000000"
       },
       forced_height = 100,
       forced_width  = 10,
       direction     = 'east',
       layout        = wibox.container.rotate,

   }

bright_widget.widget:set_value(1)

function update_brigthness(widget)
   awful.spawn.easy_async("xbacklight -get",
   function(stdout)
      local brigthness = stdout
      widget.widget:set_value(math.floor(brigthness)/100)
   end)
end

update_brigthness(bright_widget)

mytimer = timer({ timeout = 1 })
mytimer:connect_signal("timeout", function () update_brigthness(bright_widget) end)
mytimer:start()



--- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "Debian", debian.menu.Debian_menu.Debian },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar
-- Create a textclock widget
mytextclock = awful.widget.textclock("%a %d %b %X  ")

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    tags = {
        names  = { "Firefox", "Emacs", "Term", "Chats", "evolution", "spotify",7, 8, 9 },
        layout = {
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2],
               awful.layout.layouts[2]
               }
        }
 for s = 1, screen.count() do
     -- Each screen has its own tag table.
     tags[s] = awful.tag(tags.names, s, tags.layout)
 end

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            wibox.widget.systray(),
            mytextclock,
            volume_widget,
            volume_widget_bar,
            bright_image,
            bright_widget,
            wifi_signal_image,
            batterywidget_image1,
            batterywidget_image2,
            test,
            s.mylayoutbox,
        },
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(

   awful.key({ }, "XF86AudioRaiseVolume", function ()
         awful.util.spawn("amixer set Master 9%+",false) end),
   awful.key({ }, "XF86AudioLowerVolume", function ()
         awful.util.spawn("amixer set Master 9%-",false) end),
   awful.key({modkey,  }, "e", function ()
         awful.util.spawn("emacs",false) end),
   awful.key({modkey,  }, "c", function ()
         awful.util.spawn("hexchat",false) end),
   awful.key({modkey,  }, "i", function ()
         awful.util.spawn("iceweasel",false) end),
   awful.key({ }, "XF86AudioMute", function ()
         awful.util.spawn("amixer sset Master toggle",false) end),
   awful.key({ }, "XF86MonBrightnessDown", function ()
         awful.util.spawn("xbacklight -dec 5") end),
   awful.key({ }, "XF86MonBrightnessUp", function ()
         awful.util.spawn("xbacklight -inc 5") end),
    awful.key({ modkey, }, "z", function () awful.util.spawn_with_shell("i3lock -i /home/yohann/.config/awesome/wonderwoman.png -t ") end),

    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize", group = "client"})
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = 0,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     callback = awful.titlebar.remove
     }
          },
    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Don't add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },
    -- set ting to open where I want to
     { rule = { class = "Emacs" },
      properties = { tag = tags[1][2], opacity = 0.9} },
     { rule = { class = "Firefox" },
      properties = { tag = tags[1][1]} },
     { rule = { class = "Gnome-terminal" },
      properties = { tag = tags[1][3], opacity = 0.8} },
     { rule = { class = "Hexchat" },
       properties = { tag = tags[1][4] } },
     { rule = { class = "Google-chrome" },
       properties = { tag = tags[1][4] } },

     { rule = { class = "Evolution" },
      properties = { tag = tags[1][5] } },
}

-- This little bitch of spotify set it's class after draw
-- but this will teach it the good manners

client.connect_signal(
    "property::class",
    function(c)
        if c.class == "Spotify" then
        awful.client.movetotag(tags[1][6])

        end
    end)


-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.floatingbutton (c),
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
        and awful.client.focus.filter(c) then
        client.focus = c
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
